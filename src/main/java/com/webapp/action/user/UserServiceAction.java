package com.webapp.action.user;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.webapp.domain.Users;
import com.webapp.service.UserService;

public class UserServiceAction extends ActionSupport implements ModelDriven<Users> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Users users;

	@Autowired
	private UserService userService;

	public UserServiceAction() {
		users = new Users();
	}

	@Override
	public Users getModel() {
		return users;
	}

	public String processLogin() {
		return SUCCESS;
	}

	public String processLogOut() {
		return SUCCESS;
	}

	public String listUsers() {
		return SUCCESS;
	}

	public String newUser() {
		return SUCCESS;
	}

	public String editUser() {
		return SUCCESS;
	}

	public String deleteUser() {
		return SUCCESS;
	}
}
