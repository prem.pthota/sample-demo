package com.webapp.dao;

import java.util.List;

public interface GenericDao<T> {
	public void save(T t);

	public T findById(Integer id);

	public T findById(Long id);

	public List<T> findByHqlQuery(String hqlQuery);

	public void delete(T t);

	public List<T> findByHqlQueryWithParameter(String hqlQuery, String param, Object paramValue);
}
