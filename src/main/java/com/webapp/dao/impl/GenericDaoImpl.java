package com.webapp.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.springframework.orm.hibernate5.support.HibernateDaoSupport;

import com.webapp.dao.GenericDao;

public class GenericDaoImpl<T> extends HibernateDaoSupport implements GenericDao<T> {

	@Override
	public void save(T t) {
		getHibernateTemplate().saveOrUpdate(t);
	}

	@Override
	public T findById(Integer id) {
		return (T) getHibernateTemplate().get(getModelClass(), id);
	}

	@Override
	public T findById(Long id) {
		return (T) getHibernateTemplate().get(getModelClass(), id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findByHqlQuery(String hqlQuery) {
		return (List<T>) getHibernateTemplate().find(hqlQuery);
	}

	@Override
	public void delete(T t) {
		getHibernateTemplate().delete(t);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findByHqlQueryWithParameter(String hqlQuery, String param, Object paramValue) {
		return (List<T>) getHibernateTemplate().findByNamedParam(hqlQuery, param, paramValue);
	}

	@SuppressWarnings("unchecked")
	public Class<T> getModelClass() {
		return ((Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0]);

	}


}
