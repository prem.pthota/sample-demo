package com.webapp.service;

import java.util.List;

import com.webapp.domain.Users;
public interface UserService {

	public void save(Users users);

	public Users findById(Integer id);

	public List<Users> list();
	
	public void delete(Users users);
}
