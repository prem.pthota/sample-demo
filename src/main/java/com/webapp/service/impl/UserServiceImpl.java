package com.webapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.webapp.dao.UserDao;
import com.webapp.domain.Users;
import com.webapp.service.UserService;

public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public void save(Users users) {
		userDao.save(users);

	}

	@Override
	public Users findById(Integer id) {

		return userDao.findById(id);
	}

	@Override
	public List<Users> list() {

		return userDao.findByHqlQuery("from Users where status=1");
	}

	@Override
	public void delete(Users users) {
		userDao.delete(users);

	}

}
